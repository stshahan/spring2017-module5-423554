<?php
//session_save_path('/home/stshahan/public_html/module5/sesh');
session_start(); 

// This is a *good* example of how you can implement password-based user authentication in your web application.
$mysqli = new mysqli('localhost', 'root', 'saturn21', 'calendar');
 
if($mysqli->connect_errno) {
	printf("Connection Failed: %s\n", $mysqli->connect_error);
	exit;
}

// Use a prepared statement
$stmt = $mysqli->prepare("SELECT COUNT(*), userid, salt FROM userinfo WHERE username=?");
 
// Bind the parameter
$user = $_POST['username'];
$stmt->bind_param('s', $user);
$stmt->execute();
 
// Bind the results
$stmt->bind_result($cnt, $user_id, $pwd_hash);
$stmt->fetch();
 
$pwd_guess = $_POST['psw'];
// Compare the submitted password to the actual password hash
// In PHP < 5.5, use the insecure: if( $cnt == 1 && crypt($pwd_guess, $pwd_hash)==$pwd_hash){
 
if($cnt == 1 && password_verify($pwd_guess, $pwd_hash)){
	// Login succeeded!
	$_SESSION['user_id'] = $user_id;
	$_SESSION['username']=(string) $user;
	echo $user_id;
	//header("Location: calendarmain.php");
	exit;
	// Redirect to your target page
} else{
	// Login failed; redirect back to the login screen
	echo "no";
	//echo $cnt;
	//echo password_verify($pwd_guess, $pwd_hash);
	//header("Location: login.php");
	exit;
}

?>

