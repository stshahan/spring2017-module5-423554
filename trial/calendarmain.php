<!DOCTYPE html>
<html>
<head>
<title>Calendar</title>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js" type="text/javascript"></script>
</head>
<body>
<h1> Calendar Website!</h1>
<button id="btn_login">Login</button>
<button id="btn_logout">Logout</button>
<div id="loginstuff"> </div>
<a href="registerpage.php">Register</a>
<p>You Are: </p>
<div id="whoare">
<p id="usernameo"></p>
Even if you are a guest, you can use our calendar in a trial run until you press the logout button, or exit the page.

</div>
<br />
<img src="imagecal.jpg" height="100" width="100">
<br />
<strong>Year:</strong>
<button id="btn_ye1">Back a Year</button>
<p id="year">2017</p>
<button id="btn_ye2">Forward a Year</button>
</br>
<strong>Month:</strong>
<select id="month">
	<option value="Jan">January</option>
	<option value="Feb">February</option>
	<option value="Mar">March</option>
	<option value="Apr">April</option>
	<option value="May">May</option>
	<option value="Jun">June</option>
	<option value="Jul">July</option>
	<option value="Aug">August</option>
	<option value="Sep">September</option>
	<option value="Oct">October</option>
	<option value="Nov">November</option>
	<option value="Dec">December</option>

</select>
<br />
<button id="btn_1">Add Event</button>
Title: 
<input type="text" id="title"   />
Date:
<select id="date">

</select>
Time:
<select id="time">
</select>
Tag:
<input type="text" id="tagg" />
<br />
Check or uncheck tags here, then press "see tagged events" to limit what tags are visible to you.

<div id="fortag">
</div>

<table id="maintab" border="1">
	<tr>
		<th>Week</th>
		<th>Monday</th>
		<th>Tuesday</th>
		<th>Wednesday</th>
		<th>Thursday</th>
		<th>Friday</th>
		<th>Saturday</th>
		<th>Sunday</th>
	</tr>
	<tr>
		<td>Week1</td>
		<td>1</td>
		<td>2</td>
		<td>3</td>
		<td>4</td>
		<td>5</td>
		<td>6</td>
		<td>7</td>
	</tr>
	<tr>
		<td>Week2</td>
		<td>8</td>
		<td>9</td>
		<td>10</td>
		<td>11</td>
		<td>12</td>
		<td>13</td>
		<td>14</td>
	</tr>
	<tr>
		<td>Week3</td>
		<td>15</td>
		<td>16</td>
		<td>17</td>
		<td>18</td>
		<td>19</td>
		<td>20</td>
		<td>21</td>
	</tr>
	<tr>
		<td>Week4</td>
		<td>22</td>
		<td>23</td>
		<td>24</td>
		<td>25</td>
		<td>26</td>
		<td>27</td>
		<td>28</td>
	</tr>

</table>

<button id="btn_2">Delete Event</button>
<div id="fordel">
</div>

<button id="btn_mod">Modify Event</button>
<button id="btn_tag">See Tagged Events</button>
<div id="formod">
</div>

<script type="text/javascript">	

//set up some options so I don't have to type these out a million times in HTML
for(i=1; i<29; i++){
document.getElementById("date").innerHTML+='<option value="'+i+'">'+i+'</option>';
}
for(i=1; i<13; i++){
document.getElementById("time").innerHTML+='<option value="'+i+'am">'+i+'am</option>';
}
for(i=1; i<13; i++){
document.getElementById("time").innerHTML+='<option value="'+i+'pm">'+i+'pm</option>';
}


document.getElementById("fortag").innerHTML='early';

function updata(){
//go into mysql and see what events exist

var xmlHttp = new XMLHttpRequest();
		xmlHttp.open("POST", "loggedin.php", true);
		xmlHttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
		var usernan=document.getElementById("usernameo").innerHTML;
		var sendtex="loggedin.php?username="+usernan;
		//alert(sendtex);
		var responsei="";
		xmlHttp.onload = function () {
    			if (xmlHttp.readyState === xmlHttp.DONE) {
      			  if (xmlHttp.status === 200) {
				responsei=xmlHttp.responseText;
				//alert("here");
				//alert(responsei);
				if (responsei=='false'){
					alert("that's not a real username");
				}
				else{
					var js_cody2=responsei;
				
				
				
    			    
//all this is else continued   			 
var month=document.getElementById('month');
month=month.options[month.selectedIndex].value;
var respa=1;

for(i=1; i<29; i++){
	var rowy=Math.ceil(i/7);
	var colly=i-(rowy-1)*7;
	document.getElementById("maintab").rows[rowy].cells[colly].innerHTML=String(i);
}

var stringyi="displayevents_notag.php?month="+month+"&year="+document.getElementById("year").innerHTML+"&userid="+js_cody2;
//alert(stringyi);
var totaltaggies=0;
document.getElementById("fortag").innerHTML='';
//get them for a specific month, the one you're on	
	$.get(stringyi, function(result)
		{
			
			if(result==0){
				//alert("no data here");
			}
			else{
			respa=JSON.parse(result);
			var startag=[];
			for (i=0; i< respa.length; i++){
				var rowy=Math.ceil(respa[i].date/7);
				var colly=respa[i].date-(rowy-1)*7;
				//alert(rowy);
				document.getElementById("maintab").rows[rowy].cells[colly].innerHTML+="<br /><i>"+respa[i].title+"</i> at: "+respa[i].time+"<br /><u> tagged: </u>" +respa[i].tagg;
				startag.push(respa[i].tagg);
			}
			var uniquetags = [];
			$.each(startag, function(i, j){
			   	if($.inArray(j, uniquetags) === -1) uniquetags.push(j);
			});
			totaltaggies=uniquetags.length;
			for(i=0; i<uniquetags.length; i++){
				document.getElementById("fortag").innerHTML+='<input type="checkbox" name="checka" value="'+uniquetags[i]+'">'+uniquetags[i]+'<br />';			
			//	alert(document.getElementById("fortag").innerHTML);
			}
			}
		}
	);
	
//closes else
}
}
}
};

xmlHttp.send(sendtex);	
}


function softupdata(){
//go into mysql and see what events exist

var xmlHttp = new XMLHttpRequest();
		xmlHttp.open("POST", "loggedin.php", true);
		xmlHttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
		var usernan=document.getElementById("usernameo").innerHTML;
		var sendtex="loggedin.php?username="+usernan;
		//alert(sendtex);
		var responsei="";
		xmlHttp.onload = function () {
    			if (xmlHttp.readyState === xmlHttp.DONE) {
      			  if (xmlHttp.status === 200) {
				responsei=xmlHttp.responseText;
				//alert("here");
				//alert(responsei);
				if (responsei=='false'){
					alert("that's not a real username");
				}
				else{
					var js_cody2=responsei;
				
				
				
    			    
//all this is else continued   			 
var month=document.getElementById('month');
month=month.options[month.selectedIndex].value;
var respa=1;

for(i=1; i<29; i++){
	var rowy=Math.ceil(i/7);
	var colly=i-(rowy-1)*7;
	document.getElementById("maintab").rows[rowy].cells[colly].innerHTML=String(i);
}
for (i=0; i<document.getElementById("fortag").childNodes.length; i++){

stringyi="displayevents.php?month="+month+"&year="+document.getElementById("year").innerHTML+"&userid="+js_cody2;
	if(document.getElementById("fortag").childNodes[i].checked){
		stringyi=stringyi+"&tg="+document.getElementById("fortag").childNodes[i].value;

var totaltaggies=0;
//get them for a specific month, the one you're on	
	$.get(stringyi, function(result)
		{
			if(result==0){
				//alert("There is no data here");
			}
			else{
			respa=JSON.parse(result);
			//alert(respa[0].date);
			var startag=[];
			for (i=0; i< respa.length; i++){
				var rowy=Math.ceil(respa[i].date/7);
				var colly=respa[i].date-(rowy-1)*7;
				//alert(rowy);
				document.getElementById("maintab").rows[rowy].cells[colly].innerHTML+="<br /><i>"+respa[i].title+"</i> at: "+respa[i].time+"<br /><u> tagged: </u>" +respa[i].tagg;
				startag.push(respa[i].tagg);
			}
			}
		}
	);	
} //closes if
 //closes for
//closes else, below
}
}
}
};
}

xmlHttp.send(sendtex);	
}


document.getElementById("btn_1").addEventListener("click", addeve, false);
document.getElementById("month").addEventListener("change", updata, false);
//window.addEventListener("load", logoutthat, false);
window.addEventListener("load", updata, false);
document.getElementById("btn_2").addEventListener("click", deleve,false);
document.getElementById("btn_ye1").addEventListener("click",function(){
	var yara=document.getElementById("year").innerHTML;
	document.getElementById("year").innerHTML=String(parseInt(yara)-1);
	updata();
	
}, false);
document.getElementById("btn_ye2").addEventListener("click",function(){
	var yara=document.getElementById("year").innerHTML;
	document.getElementById("year").innerHTML=String(parseInt(yara)+1);
	updata();
	
}, false);
document.getElementById("btn_login").addEventListener("click", loginthatsucka, false);
document.getElementById("btn_logout").addEventListener("click", logoutthat, false);

function logoutthat(){
	alert("logout");
	var xmlHttp = new XMLHttpRequest();
	xmlHttp.open("POST", "logout.php", true);
	xmlHttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	var sendtex2="logout.php?userid=3";
	xmlHttp.send(sendtex2);	



	updata();
		
}
document.getElementById("btn_mod").addEventListener("click", modit, false);
document.getElementById("btn_tag").addEventListener("click", tagcha, false);

function tagcha(){
	//alert(document.getElementById("fortag").innerHTML);
	softupdata();

}

//modifies an already created event
function modit(){
	var month=document.getElementById('month');
	month=month.options[month.selectedIndex].value;
	var yeara2=document.getElementById("year").innerHTML;

	var xmlHttp = new XMLHttpRequest();
		xmlHttp.open("POST", "loggedin.php", true);
		xmlHttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
		var usernan=document.getElementById("usernameo").innerHTML;
		var sendtex="loggedin.php?username="+usernan;
		//alert(sendtex);
		var responsei="";
		xmlHttp.onload = function () {
    			if (xmlHttp.readyState === xmlHttp.DONE) {
      			  if (xmlHttp.status === 200) {
				responsei=xmlHttp.responseText;
				//alert("here");
				//alert(responsei);
				if (responsei=='false'){
					alert("that's not a real username");
				}
				else{
					var js_cody2=responsei;

	var stringforsel='<select id="modchoice">';
	var stringyi="displayevents_notag.php?month="+month+"&year="+document.getElementById("year").innerHTML+"&userid="+js_cody2;
	var cloneda=document.getElementById("date").cloneNode(true);
	var cloneda2=document.getElementById("time").cloneNode(true);
	$.get(stringyi, function(result)
		{
			respa=JSON.parse(result);
			//alert(respa[0].date);
			for (i=0; i< respa.length; i++){
				stringforsel=stringforsel+"<option value="+i+">"+respa[i].title+respa[i].time+month+respa[i].date+"</option>";
				//document.getElementById("maintab").rows[rowy].cells[colly].innerHTML+="<br /><i>"+respa[i].title+"</i> at: "+respa[i].time;
			}
			stringforsel=stringforsel+"</select>";	
			//alert(stringforsel);
			document.getElementById("formod").innerHTML=stringforsel;
			document.getElementById("formod").innerHTML+='<button id="btn_3b">Modify THIS event</button>';
			document.getElementById("formod").innerHTML+='<button id="btn_4b">JK please do not modify anything</button>';
			document.getElementById("btn_4b").addEventListener("click",function(){
					document.getElementById('formod').innerHTML="";	
				}, false);
			document.getElementById("btn_3b").addEventListener("click", function(){
				//create option to change title
				document.getElementById("formod").innerHTML+='<button id="btn_3c">Change it</button><input type="text" id="modte">New Title Here (if empty, will stay same)</input>';
				//add date
				document.getElementById("formod").appendChild(cloneda);
				document.getElementById("formod").appendChild(cloneda2);
				document.getElementById("formod").childNodes[6].innerHTML+='<option value="same">Same as before</option>';
				document.getElementById("formod").childNodes[7].innerHTML+='<option value="same">Same as before</option>';
				var choica=document.getElementById('modchoice');
				document.getElementById("btn_3c").addEventListener("click", function(){
				var xmlHttp2 = new XMLHttpRequest();
				xmlHttp2.open("POST", "modifyevents.php", true);
				xmlHttp2.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
				//here insert month, date, etc
				var tima=respa[choica.options[choica.selectedIndex].value].time;
				var tita=respa[choica.options[choica.selectedIndex].value].title;
				var dati=respa[choica.options[choica.selectedIndex].value].date;
				var yara3=document.getElementById("year").innerHTML;
				var js_cody4=respa[choica.options[choica.selectedIndex].value].userid;
				var titan=document.getElementById("modte").value;
				if (titan==''){
					titan=tita;
				}	
				var datin=document.getElementById("formod").childNodes[6].value;
				var timan=document.getElementById("formod").childNodes[7].value;
				if (datin=="same"){
					datin=dati;
				}
				if (timan=="same"){
					timan=tima;
				}
				var sendtex2="title="+tita+"&time="+tima+"&userni=1&month="+String(month)+"&date="+dati+"&year="+yara3+"&userid="+js_cody4+"&titlen="+titan+"&timen="+timan+"&usernin=1&monthn="+String(month)+"&daten="+datin+"&yearn="+yara3+"&useridn="+js_cody4;
				//alert(sendtex2);
				xmlHttp2.send(sendtex2);	
				document.getElementById('formod').innerHTML="";	
				updata();
				}, false);	

			}, false);
		}
	);
	}
}
}
};	
	//update calendar
	xmlHttp.send(sendtex);
	updata();
}




//adds an event into the mysql database
function loginthatsucka(){
	document.getElementById("loginstuff").innerHTML='User name: <br> <input type="text" name="username" id="usernamefi"> <br> User password: <br> <input type="password" name="psw" id="pswa">  <button id="btn_logi">Login Now</button>';	
	document.getElementById("btn_logi").addEventListener("click", function(){
		
		var xmlHttp = new XMLHttpRequest();
		xmlHttp.open("POST", "loginrun.php", true);
		xmlHttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
		var usernan=document.getElementById("usernamefi").value;
		var passy=document.getElementById("pswa").value;
		var sendtex="loginrun.php?username="+usernan+"&psw="+passy;
		//alert("watch");
		//alert(sendtex);
		var responsei="";
		xmlHttp.onload = function () {
    			if (xmlHttp.readyState === xmlHttp.DONE) {
      			  if (xmlHttp.status === 200) {
				responsei=xmlHttp.responseText;
				if (responsei=='no'){
					alert("that's not a real username");
				}
				else{
					
				}
				updata();
				
    			    }
   			 }
		};
		xmlHttp.send(sendtex);
		document.getElementById("loginstuff").innerHTML='';
		
	
	}, false);

}

//adds an event for a user, inserts into MySql database
function addeve(){
	var xmlHttp = new XMLHttpRequest();
	xmlHttp.open("POST", "submitrun.php", true);
	xmlHttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	var month=document.getElementById('month');
	month=month.options[month.selectedIndex].value;
	var tima=document.getElementById('time');
	tima=tima.options[tima.selectedIndex].value;
	var tita=document.getElementById('title').value;
	var taga=document.getElementById('tagg').value;
	if(taga==''){
		taga='notags';
	}
	else if(taga=='notags'){
		taga='tag_is_notags';
	}
	var dati=document.getElementById('date');
	dati=dati.options[dati.selectedIndex].value;
	var yeara=document.getElementById("year").innerHTML;
		
	var sendtex="title="+tita+"&time="+tima+"&userni=1&month="+String(month)+"&date="+dati+"&year="+yeara+"&userid="+2+"&tagg="+taga;
	//alert(sendtex);
	xmlHttp.send(sendtex);
	updata();
	
}

//deletes an event, also creates HTML entities to help a person delete an event
function deleve(){
	var month=document.getElementById('month');
	month=month.options[month.selectedIndex].value;
	var yeara2=document.getElementById("year").innerHTML;
	var stringforsel='<select id="delchoice">';	
	var xmlHttp = new XMLHttpRequest();
		xmlHttp.open("POST", "loggedin.php", true);
		xmlHttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
		var usernan=document.getElementById("usernameo").innerHTML;
		var sendtex="loggedin.php?username="+usernan;
		
		var responsei="";
		xmlHttp.onload = function () {
    			if (xmlHttp.readyState === xmlHttp.DONE) {
      			  if (xmlHttp.status === 200) {
				responsei=xmlHttp.responseText;
				
				//alert(responsei);
				if (responsei=='false'){
					alert("that's not a real username");
				}
				else{
					var js_cody2=responsei;
			var stringyi="displayevents_notag.php?month="+month+"&year="+document.getElementById("year").innerHTML+"&userid="+js_cody2;
		//alert(stringyi);
	$.get(stringyi, function(result)
		{

			respa=JSON.parse(result);
			//alert(respa);
			//alert(respa[0].date);
			for (i=0; i< respa.length; i++){
				stringforsel=stringforsel+"<option value="+i+">"+respa[i].title+respa[i].time+month+respa[i].date+"</option>";
				//document.getElementById("maintab").rows[rowy].cells[colly].innerHTML+="<br /><i>"+respa[i].title+"</i> at: "+respa[i].time;
			}
			stringforsel=stringforsel+"</select>";	
			//alert(stringforsel);
			document.getElementById("fordel").innerHTML=stringforsel;
			document.getElementById("fordel").innerHTML+='<button id="btn_3">Delete THIS event</button>';
			document.getElementById("fordel").innerHTML+='<button id="btn_4">JK please do not delete anything</button>';
			document.getElementById("btn_4").addEventListener("click",function(){
					document.getElementById('fordel').innerHTML="";	
				}, false);
			document.getElementById("btn_3").addEventListener("click", function(){
				var choica=document.getElementById('delchoice');
				//alert(respa[choica.options[choica.selectedIndex].value].time);
				
				var xmlHttp2 = new XMLHttpRequest();
				xmlHttp2.open("POST", "deleteevents.php", true);
				xmlHttp2.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
				//here insert month, date, etc
				var tima=respa[choica.options[choica.selectedIndex].value].time;
				var tita=respa[choica.options[choica.selectedIndex].value].title;
				var dati=respa[choica.options[choica.selectedIndex].value].date;
				var usaa=respa[choica.options[choica.selectedIndex].value].userid;
				var yara3=document.getElementById("year").innerHTML;
				var sendtex2="title="+tita+"&time="+tima+"&userni=1&month="+String(month)+"&date="+dati+"&year="+yara3+"&userid="+usaa;
				//alert(sendtex2);
				xmlHttp2.send(sendtex2);	
				document.getElementById('fordel').innerHTML="";	
				updata();	

			}, false);
			//alert("going yes");
		}
	);
	
//closes else
}
}
}
};

xmlHttp.send(sendtex);	
updata();
}
	



</script>

</body>
</html>